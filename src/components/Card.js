import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
} from 'react-native';

import * as Material from 'react-native-material-ui';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  card: {
    flex: 1,
    width: width - 15,
    height: width,
  },
  image: {
    flex: 2,
    justifyContent: 'center',
  },
  cardDataContainer: {
    flex: 1,
    paddingVertical: 24,
    paddingHorizontal: 16,
  },
  title: {
    height: 50,
    lineHeight: 50,
    fontSize: 24,
  },
  text: {
    flex: 1,
    fontSize: 14,
  },
});

export default class Card extends React.Component {
  render() {
    const {
      id,
      img,
      title,
      text,
    } = this.props.product;

    const {
      onPress,
    } = this.props;

    return (
      <View style={styles.container}>
        <Material.Card onPress={onPress}>
          <View style={styles.card}>
            <Image
              resizeMode={'contain'}
              source={{ uri: `http://smktesting.herokuapp.com/static/${img}`, static: true }}
              style={styles.image}
            />
            <View style={styles.cardDataContainer}>
              <Text style={styles.title}>{title}</Text>
              <Text style={styles.text}>{text}</Text>
            </View>
          </View>
        </Material.Card>
      </View>
    );
  }
}
