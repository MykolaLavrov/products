export const color = {
  primary: '#00BFA5',
  primaryLight: '#1DE9B6',
  red: 'red',
  yellow: 'yellow',
  gray: '#aaa',
  black: '#000',
  grayLight: '#eee',
  white: '#fff',
  transparent: 'transparent',
  background: '#ccc',
};
