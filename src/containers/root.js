import React from 'react';
import {
  View,
  StyleSheet,
  StatusBar,
} from 'react-native';
import { COLOR, ThemeProvider } from 'react-native-material-ui';
import { connect } from 'react-redux';
import LoadingSpinnerOverlay from 'react-native-smart-loading-spinner-overlay';

import RouterComponent from '../route/RouterComponent';
import * as actionsAuth from '../redux/actions/actionsAuth';
import * as actionsProducts from '../redux/actions/actionsProducts';

console.disableYellowBox = true;

const uiTheme = {
  palette: {
    primaryColor: COLOR.green500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: uiTheme.palette.primaryColor,
  },
});

class Root extends React.Component {
  componentWillMount() {
    const { getProductsAction, loadDataFromStorage } = this.props;
    loadDataFromStorage();
    getProductsAction();
  }

  componentWillReceiveProps(nextProps) {
    const { authRequestInProgress, authResult } = this.props;

    if (!authRequestInProgress && nextProps.authRequestInProgress) {
      this.showModalLoadingSpinnerOverLay();
    }
    if (authRequestInProgress && !nextProps.authRequestInProgress) {
      this.hideModalLoadingSpinnerOverLay();
    }
  }

  showModalLoadingSpinnerOverLay = () => {
    this._modalLoadingSpinnerOverLay.show();
  };

  hideModalLoadingSpinnerOverLay = () => {
    this._modalLoadingSpinnerOverLay.hide();
  };

  render() {
    return (
      <ThemeProvider uiTheme={uiTheme}>
        <View style={styles.container}>
          <StatusBar
            barStyle="light-content"
            animated
            hidden
          />
          <RouterComponent />
          <LoadingSpinnerOverlay
            ref={component => this._modalLoadingSpinnerOverLay = component}
          />
        </View>
      </ThemeProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    username: state.reducerAuth.username,
    authRequestInProgress: state.reducerAuth.authRequestInProgress,
    authResult: state.reducerAuth.authResult,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getProductsAction: () => { dispatch(actionsProducts.getProductsAction()); },
    loadDataFromStorage: () => { dispatch(actionsAuth.loadDataFromStorage()); },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Root);
