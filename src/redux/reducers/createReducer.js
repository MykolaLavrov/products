import { combineReducers } from 'redux';
import reducerAuth from './reducerAuth';
import reducerProducts from './reducerProducts';

const reducers = {
  reducerAuth,
  reducerProducts,
};

const appReducer = combineReducers(reducers);

const rootReducer = (state, action) => appReducer(state, action);

export default rootReducer;
