import React from 'react';
import {
  StyleSheet,
  View,
  ListView,
} from 'react-native';
import { connect } from 'react-redux';
import { Toolbar } from 'react-native-material-ui';

import * as actionsRoute from '../redux/actions/actionsRoute';
import * as actionsProducts from '../redux/actions/actionsProducts';
import Card from '../components/Card';
import { color } from '../constants/color';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.background,
  },
});

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class Products extends React.Component {
  onPressMenu = () => {
    actionsRoute.toggleDrawer();
  };
  onCardPress = (data) => {
    const { onCardPressAction } = this.props;
    onCardPressAction(data);
  };
  render() {
    const { products } = this.props;
    return (
      <View style={styles.container}>
        <Toolbar
          leftElement="menu"
          onLeftElementPress={this.onPressMenu}
          style={{
            container: {
              backgroundColor: color.primary,
            },
          }}
        />
        <ListView
          style={styles.container}
          dataSource={ds.cloneWithRows(products)}
          enableEmptySections
          onRefresh={this.onRefresh}
          renderRow={rowData => (<Card
            product={rowData}
            onPress={() => { this.onCardPress(rowData); }}
          />)}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    products: state.reducerProducts.products,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onCardPressAction: (data) => { dispatch(actionsProducts.onCardPressAction(data)); },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Products);
