import React from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  Dimensions,
  Image,
  Modal,
  InteractionManager,
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import StarRating from 'react-native-star-rating';
import { Toolbar, ActionButton } from 'react-native-material-ui';

import * as actionsRoute from '../redux/actions/actionsRoute';
import * as actionsProducts from '../redux/actions/actionsProducts';
import Spinner from '../components/Spinner';
import Review from '../pages/Review';
import { color } from '../constants/color';

const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flex: 1,
  },
  image: {
    width,
    height: height / 3,
  },
  title: {
    width,
    fontSize: 20,
    paddingHorizontal: 16,
    paddingVertical: 24,
  },
  text: {
    width,
    fontSize: 16,
    paddingHorizontal: 16,
    paddingBottom: 24,
  },
  reviewContainer: {
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: color.gray,
    paddingVertical: 12,
  },
  username: {
    fontSize: 18,
    fontWeight: 'bold',
    marginHorizontal: 16,
  },
  created_at: {
    fontSize: 14,
    marginHorizontal: 16,
    color: color.gray,
  },
  starRating: {
    width: 100,
    height: 35,
    justifyContent: 'center',
    marginHorizontal: 16,
    marginVertical: 10,
  },
  reviewText: {
    marginHorizontal: 16,
  },
  toolbarContainerView: {
    position: 'absolute',
    top: 0,
    left: 0,
  },
});

class ProductDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showEmptyScreen: true,
      starCount: 0,
      modalVisible: false,
    };
  }

  componentDidMount() {
    InteractionManager.runAfterInteractions(() => {
      this.setState({ showEmptyScreen: false });
    });
  }

  onLeftElementPress = () => {
    actionsRoute.pop();
  };

  onStarRatingPress = (rating) => {
    this.setState({ starCount: rating });
  };

  onPressActionButton = (text) => {
    if (text === 'star') {
      this.setModalVisible(true);
    }
  };

  setModalVisible = (value) => {
    this.setState({ modalVisible: value });
  };

  render() {
    const { showEmptyScreen } = this.state;
    const { product, productReviews } = this.props;
    return (
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Image
            resizeMode={'contain'}
            source={{ uri: `http://smktesting.herokuapp.com/static/${product.img}`, static: true }}
            style={styles.image}
          />
          <View style={styles.container}>
            <Text style={styles.title}>{product.title}</Text>
            <Text style={styles.text}>{product.text}</Text>
          </View>
          {
            !showEmptyScreen &&
            productReviews && productReviews.map(review => (<View key={review.id} style={styles.reviewContainer}>
              <Text style={styles.username}>{review.created_by.username}</Text>
              <Text style={styles.created_at}>{moment(review.created_at).format('MMMM Do YYYY, h:mm:ss a')}</Text>
              <View style={styles.starRating}>
                <StarRating
                  disabled
                  maxStars={5}
                  starColor={color.primaryLight}
                  starSize={20}
                  rating={review.rate}
                  emptyStar={'ios-star-outline'}
                  fullStar={'ios-star'}
                  halfStar={'ios-star-half'}
                  iconSet={'Ionicons'}
                />
              </View>
              <Text style={styles.reviewText}>{review.text}</Text>
            </View>))
          }
          <View style={styles.reviewContainer} />
          {
            showEmptyScreen && <Spinner />
          }
        </ScrollView>
        <View style={styles.toolbarContainerView}>
          <Toolbar
            leftElement="arrow-back"
            onLeftElementPress={this.onLeftElementPress}
            style={{
              container: {
                backgroundColor: color.transparent,
              },
              leftElement: {
                color: color.primary,
              },
            }}
          />
        </View>
        <ActionButton
          actions={[{ icon: 'star', label: 'Send review' }]}
          icon="star"
          transition="speedDial"
          style={{
            container: {
              backgroundColor: color.primary,
            },
          }}
          onPress={(text) => { this.onPressActionButton(text); }}
        />
        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          // onRequestClose={() => { console.warn("Modal has been closed.")}}
        >
          <Review
            closeModal={() => { this.setModalVisible(!this.state.modalVisible); }}
            onStarRatingPress={rating => this.onStarRatingPress(rating)}
            starCount={this.state.starCount}
          />
        </Modal>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    product: state.reducerProducts.product,
    productReviews: state.reducerProducts.productReviews,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    getProductReviewsAction: (id) => { dispatch(actionsProducts.getProductReviewsAction(id)); },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);
