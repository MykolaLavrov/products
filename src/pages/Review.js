import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import { connect } from 'react-redux';
import StarRating from 'react-native-star-rating';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { Toolbar } from 'react-native-material-ui';

import * as actionsProducts from '../redux/actions/actionsProducts';
import { color } from '../constants/color';

const { width, height } = Dimensions.get('window');

const appStyles = StyleSheet.create({
  btnLoginContainer: {
    width: width - 40,
    height: 45,
    backgroundColor: color.primary,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 2,
  },
  btnText: {
    fontSize: 18,
    fontWeight: '700',
    color: color.white,
  },
  shadow: {
    elevation: 5,
    shadowColor: color.black,
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 4,
      width: 1,
    },
  },
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
  },
  titleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputContainer: {
    width,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  inputTextContainer: {
    height: 130,
    width,
  },
  txtphoneContainer: {
    flex: 1,
  },
  btnContainer: {
    flex: 1,
    width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    paddingHorizontal: 20,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'center',
  },
});

const { State: TextInputState } = TextInput;

class Review extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  onSendReview = () => {
    const { onSendReviewPressAction, starCount, closeModal } = this.props;
    const { text } = this.state;
    onSendReviewPressAction({ rate: starCount, text });
    // closeModal();
  };

  dismissKeyboard() {
    TextInputState.blurTextInput(TextInputState.currentlyFocusedField());
  }

  render() {
    const { closeModal, onStarRatingPress, starCount } = this.props;
    const { text } = this.state;
    return (
      <View style={styles.container}>
        <Toolbar
          leftElement="close"
          onLeftElementPress={closeModal}
          centerElement="Review"
          style={{
            container: {
              backgroundColor: color.transparent,
            },
            leftElement: {
              color: color.primary,
            },
            titleText: {
              color: color.primary,
            },
          }}
        />
        <View style={styles.contentContainer}>
          <TouchableWithoutFeedback onPress={this.dismissKeyboard}>
            <View style={styles.contentContainer}>
              <View style={styles.titleContainer}>
                <View style={styles.starRating}>
                  <StarRating
                    disabled={false}
                    emptyStar={'ios-star-outline'}
                    fullStar={'ios-star'}
                    halfStar={'ios-star-half'}
                    iconSet={'Ionicons'}
                    maxStars={5}
                    rating={starCount}
                    selectedStar={rating => onStarRatingPress(rating)}
                    starColor={color.primaryLight}
                  />
                </View>
              </View>
              <View style={styles.inputContainer}>
                <View style={styles.inputTextContainer}>
                  <View style={styles.txtphoneContainer}>
                    <TextInput
                      multiline
                      numberOfLines={10}
                      underlineColorAndroid={color.transparent}
                      style={styles.textInput}
                      placeholder="Please, write your feedback"
                      placeholderTextColor={color.gray}
                      onChangeText={text_ => this.setState({ text: text_ })}
                      autoCapitalize="none"
                      value={text}
                    />
                  </View>
                </View>
              </View>
              <View style={[styles.btnContainer]}>
                <TouchableOpacity
                  style={[appStyles.btnLoginContainer, appStyles.shadow]}
                  onPress={this.onSendReview}
                >
                  <Text style={[appStyles.btnText]}>
                    SEND REVIEW
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <KeyboardSpacer />
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    onSendReviewPressAction: (data) => { dispatch(actionsProducts.onSendReviewPressAction(data)); },
  };
}

export default connect(null, mapDispatchToProps)(Review);
