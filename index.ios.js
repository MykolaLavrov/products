import { AppRegistry } from 'react-native';
import App from './src/containers/';

AppRegistry.registerComponent('Products', () => App);
